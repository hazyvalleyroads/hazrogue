const std = @import("std");
const engine = @import("engine.zig");
const c = engine.c;

const Game = engine.Game;

pub fn main() !void {
    // Set up allocator
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    var allocator = &arena.allocator;

    // Set up and run game
    var game = try Game.init("Hazrogue", 40, 25, allocator);
    defer game.deinit();
    try game.mainLoop();
}
