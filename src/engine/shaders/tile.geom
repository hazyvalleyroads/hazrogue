#version 330 core

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

in float v_tileset[];
in float v_alpha[];
in vec2 v_tex_coord_offset[];
in vec3 v_color[];

out float g_tileset;
out float g_alpha;
out vec2 g_tex_coord;
out vec3 g_color;

uniform vec3 u_grid_size;

vec2 offset(vec2 tex_coord, vec2 tex_offset) {
    return tex_coord / 16.0 + tex_offset;
}

void main() {
    vec4 position = gl_in[0].gl_Position;
    vec2 tile = 2.0 / u_grid_size.xy;

    g_tileset = v_tileset[0];
    g_alpha = v_alpha[0];
    g_color = v_color[0];

    gl_Position = position + vec4(0.0, 0.0, 0.0, 0.0); // 1:bottom-left
    g_tex_coord = offset(vec2(0.0, 0.0), v_tex_coord_offset[0]);
    EmitVertex();
    gl_Position = position + vec4( tile.x, 0.0, 0.0, 0.0); // 2:bottom-right
    g_tex_coord = offset(vec2(1.0, 0.0), v_tex_coord_offset[0]);
    EmitVertex();
    gl_Position = position + vec4(0.0,  tile.y, 0.0, 0.0); // 3:top-left
    g_tex_coord = offset(vec2(0.0, 1.0), v_tex_coord_offset[0]);
    EmitVertex();
    gl_Position = position + vec4( tile.x,  tile.y, 0.0, 0.0); // 4:top-right
    g_tex_coord = offset(vec2(1.0, 1.0), v_tex_coord_offset[0]);
    EmitVertex();

    EndPrimitive();
}
