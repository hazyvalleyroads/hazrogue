#version 330 core

layout (location = 0) in float a_tileset;
layout (location = 1) in float a_alpha;
layout (location = 2) in float a_char;
layout (location = 3) in vec3 a_color;

out float v_tileset;
out float v_alpha;
out vec2 v_tex_coord_offset;
out vec3 v_color;

uniform vec3 u_grid_size;

void main() {
    float local_idx = mod(gl_InstanceID, u_grid_size.x * u_grid_size.y);
    float x = mod(local_idx, u_grid_size.x);
    vec2 pos = vec2(x, (local_idx - x) / u_grid_size.x);
    gl_Position = vec4(pos / u_grid_size.xy * 2.0 - 1.0, 0.0, 1.0);

    v_tileset = a_tileset;
    v_alpha = a_alpha;

    v_color = a_color;
    v_tex_coord_offset = vec2(
        mod(a_char, 16.0),
        15.0 - floor(a_char / 16.0)
    ) / 16.0;
}
