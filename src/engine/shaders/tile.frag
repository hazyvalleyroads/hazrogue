#version 330 core

in float g_tileset;
in float g_alpha;
in vec2 g_tex_coord;
in vec3 g_color;

out vec4 f_color;

uniform sampler2DArray u_tex;

void main() {
    vec4 tex_color = texture(u_tex, vec3(g_tex_coord, g_tileset));
    f_color = tex_color * vec4(g_color, g_alpha);
}
