const std = @import("std");
const c = @import("c.zig");

pub const OpenGlError = error{
    LoadImageFailed,
    CompileShaderFailed,
    LinkShaderFailed,
};

/// Compiles and links a shader program from the given shader source strings
/// Returns the OpenGL program object or an error if it failed.
pub fn load_shader(vert_source: [:0]const u8, geom_source: ?[:0]const u8, frag_source: [:0]const u8) !c_uint {
    var success: c_int = undefined;
    var info_log: [512]u8 = undefined;

    // Compile vertex shader
    const vert_shader = c.glCreateShader(c.GL_VERTEX_SHADER);
    defer c.glDeleteShader(vert_shader);
    c.glShaderSource(vert_shader, 1, &vert_source.ptr, null);
    c.glCompileShader(vert_shader);

    // Check vertex compile status
    c.glGetShaderiv(vert_shader, c.GL_COMPILE_STATUS, &success);
    if (success == 0) {
        c.glGetShaderInfoLog(vert_shader, 512, null, &info_log);
        std.log.err("Vertex shader compile failed:\n{s}", .{info_log});
        return error.CompileShaderFailed;
    }

    // Compile geometry shader if it is given
    const geom_shader = c.glCreateShader(c.GL_GEOMETRY_SHADER);
    defer c.glDeleteShader(geom_shader);
    if (geom_source != null) {
        c.glShaderSource(geom_shader, 1, &(geom_source.?).ptr, null);
        c.glCompileShader(geom_shader);

        // Check vertex compile status
        c.glGetShaderiv(geom_shader, c.GL_COMPILE_STATUS, &success);
        if (success == 0) {
            c.glGetShaderInfoLog(geom_shader, 512, null, &info_log);
            std.log.err("Geometry shader compile failed:\n{s}", .{info_log});
            return error.CompileShaderFailed;
        }
    }

    // Compile fragment shader
    const frag_shader = c.glCreateShader(c.GL_FRAGMENT_SHADER);
    defer c.glDeleteShader(frag_shader);
    c.glShaderSource(frag_shader, 1, &frag_source.ptr, null);
    c.glCompileShader(frag_shader);

    // Check vertex compile status
    c.glGetShaderiv(frag_shader, c.GL_COMPILE_STATUS, &success);
    if (success == 0) {
        c.glGetShaderInfoLog(frag_shader, 512, null, &info_log);
        std.log.err("Fragment shader compile failed:\n{s}", .{info_log});
        return error.CompileShaderFailed;
    }

    // Link shader
    const shader_program = c.glCreateProgram();
    c.glAttachShader(shader_program, vert_shader);
    if (geom_source != null) {
        c.glAttachShader(shader_program, geom_shader);
    }
    c.glAttachShader(shader_program, frag_shader);
    c.glLinkProgram(shader_program);

    // Check link status
    c.glGetProgramiv(shader_program, c.GL_LINK_STATUS, &success);
    if (success == 0) {
        c.glGetProgramInfoLog(shader_program, 512, null, &info_log);
        std.log.err("Shader link failed:\n{s}", .{info_log});
        return error.LinkShaderFailed;
    }

    return shader_program;
}

/// Loads an image from the specified path and uploads it to a texture.
/// Returns the OpenGL texture object or an error if the image failed to load.
pub fn load_texture(path: [*:0]const u8) !c_uint {
    // Load the image from file
    var width: c_int = undefined;
    var height: c_int = undefined;
    var channels: c_int = undefined;
    c.stbi_set_flip_vertically_on_load(1);
    const image = c.stbi_load(path, &width, &height, &channels, 4);
    if (image == null) {
        return error.LoadImageFailed;
    }
    defer c.stbi_image_free(image);

    // Create a texture object
    var texture: c_uint = undefined;
    c.glGenTextures(1, &texture);
    c.glBindTexture(c.GL_TEXTURE_2D, texture);

    // Set filtering parameters

    c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_MIN_FILTER, c.GL_NEAREST);
    c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_MAG_FILTER, c.GL_NEAREST);

    // Upload texture data
    c.glTexImage2D(c.GL_TEXTURE_2D, 0, c.GL_RGBA, width, height, 0, c.GL_RGBA, c.GL_UNSIGNED_BYTE, image);
    return texture;
}

/// Orthographic projection matrix
pub fn ortho(left: f32, right: f32, bottom: f32, top: f32, znear: f32, zfar: f32) [4][4]f32 {
    const a = 2.0 / (right - left);
    const b = 2.0 / (top - bottom);
    const k = -2.0 / (zfar - znear);
    const d = -(right + left) / (right - left);
    const e = -(top + bottom) / (top - bottom);
    const f = -(zfar + znear) / (zfar - znear);

    return [4][4]f32{
        .{ a, 0.0, 0.0, 0.0 },
        .{ 0.0, b, 0.0, 0.0 },
        .{ 0.0, 0.0, k, -1.0 },
        .{ d, e, f, 0.0 },
    };
}
