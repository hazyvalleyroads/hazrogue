const std = @import("std");
const c = @import("c.zig");
const gl = @import("gl.zig");

const Allocator = std.mem.Allocator;
const ArrayList = std.ArrayList;

const Self = @This();

pub const TileError = error{
    PositionOutOfBounds,
    ImageResizeFailed,
    NoImagesLoaded,
};

/// GPU Representation of a single tile.
const CellVertex = extern struct {
    tile: u8 = 3, // ID of the tile in tileset to draw
    color: [3]u8 = .{ 127, 255, 127 }, // Color of the tile
};

/// Per-layer vertex data.
const LayerVertex = extern struct {
    tileset: u8 = 0, // ID of the tileset to use for this layer
    alpha: u8 = 255, // Alpha of the entire layer.
};

// Shader source code
const vert_shader_source = @embedFile("shaders/tile.vert");
const geom_shader_source = @embedFile("shaders/tile.geom");
const frag_shader_source = @embedFile("shaders/tile.frag");

shader: c_uint,
vao: c_uint,
layers_vbo: c_uint,
cells_vbo: c_uint,
tileset: Tileset,

grid_width: usize,
grid_height: usize,
num_layers: usize,

layers: []LayerVertex,
cells: []CellVertex,
is_dirty: bool,

allocator: *Allocator,

/// Initializes a `TileRenderer` of given width and height.
pub fn init(grid_width: usize, grid_height: usize, num_layers: usize, tileset_path: []const u8, allocator: *Allocator) !Self {
    // Initialize layer and cell data slices
    var layers = try allocator.alloc(LayerVertex, num_layers);
    for (layers) |*layer| {
        layer.* = LayerVertex{};
    }
    layers[1].tileset = 1;

    var cells = try allocator.alloc(CellVertex, grid_width * grid_height * num_layers);
    for (cells) |*cell, i| {
        cell.* = CellVertex{};
        if (i >= grid_width * grid_height) {
            cell.color = .{ 127, 255, 255 };
        }
    }

    // Load shader
    const shader = try gl.load_shader(vert_shader_source, geom_shader_source, frag_shader_source);

    // Generate array and buffer objects
    var vao: c_uint = undefined;
    var layers_vbo: c_uint = undefined;
    var cells_vbo: c_uint = undefined;
    c.glGenVertexArrays(1, &vao);
    c.glGenBuffers(1, &layers_vbo);
    c.glGenBuffers(1, &cells_vbo);

    // Set up vao
    c.glBindVertexArray(vao);

    // Upload to layers vbo
    c.glBindBuffer(c.GL_ARRAY_BUFFER, layers_vbo);
    c.glBufferData(c.GL_ARRAY_BUFFER, @intCast(c_long, layers.len * @sizeOf(LayerVertex)), layers.ptr, c.GL_DYNAMIC_DRAW);

    // Layer tileset attrib
    c.glVertexAttribPointer(0, 1, c.GL_UNSIGNED_BYTE, c.GL_FALSE, @sizeOf(LayerVertex), @intToPtr(?*c_void, @offsetOf(LayerVertex, "tileset")));
    c.glVertexAttribDivisor(0, @intCast(c_uint, grid_width * grid_height));
    c.glEnableVertexAttribArray(0);

    // Layer alpha attrib
    c.glVertexAttribPointer(1, 1, c.GL_UNSIGNED_BYTE, c.GL_TRUE, @sizeOf(LayerVertex), @intToPtr(?*c_void, @offsetOf(LayerVertex, "alpha")));
    c.glVertexAttribDivisor(1, @intCast(c_uint, grid_width * grid_height));
    c.glEnableVertexAttribArray(1);

    // Upload to cells vbo
    c.glBindBuffer(c.GL_ARRAY_BUFFER, cells_vbo);
    c.glBufferData(c.GL_ARRAY_BUFFER, @intCast(c_long, cells.len * @sizeOf(CellVertex)), cells.ptr, c.GL_STREAM_DRAW);

    // Tile ID attrib
    c.glVertexAttribPointer(2, 1, c.GL_UNSIGNED_BYTE, c.GL_FALSE, @sizeOf(CellVertex), @intToPtr(?*c_void, @offsetOf(CellVertex, "tile")));
    c.glVertexAttribDivisor(2, 1);
    c.glEnableVertexAttribArray(2);

    // Color attrib
    c.glVertexAttribPointer(3, 3, c.GL_UNSIGNED_BYTE, c.GL_TRUE, @sizeOf(CellVertex), @intToPtr(?*c_void, @offsetOf(CellVertex, "color")));
    c.glVertexAttribDivisor(3, 1);
    c.glEnableVertexAttribArray(3);

    // Load tilesets
    const tileset = try loadTilesets(tileset_path, allocator);

    // Set up uniforms
    c.glUseProgram(shader);
    c.glUniform1i(c.glGetUniformLocation(shader, "u_tex"), 0);
    c.glUniform3f(c.glGetUniformLocation(shader, "u_grid_size"), @intToFloat(f32, grid_width), @intToFloat(f32, grid_height), @intToFloat(f32, num_layers));

    // Return struct
    return Self{
        .shader = shader,
        .vao = vao,
        .layers_vbo = layers_vbo,
        .cells_vbo = cells_vbo,
        .tileset = tileset,

        .grid_width = grid_width,
        .grid_height = grid_height,
        .num_layers = num_layers,

        .layers = layers,
        .cells = cells,
        .is_dirty = false,

        .allocator = allocator,
    };
}

/// Cleans up OpenGL objects and deinitializes heap shit.
pub fn deinit(self: *Self) void {
    c.glDeleteTextures(1, &self.tileset.texture);
    c.glDeleteProgram(self.shader);
    c.glDeleteVertexArrays(1, &self.vao);
    c.glDeleteBuffers(1, &self.cells_vbo);
    c.glDeleteBuffers(1, &self.layers_vbo);

    self.allocator.free(self.cells);
    self.allocator.free(self.layers);

    self.* = undefined;
}

/// Draws the tile grid to the screen.
pub fn draw(self: *Self) void {
    // Upload changed cells to GPU
    if (self.is_dirty) {
        c.glBindBuffer(c.GL_ARRAY_BUFFER, self.cells_vbo);
        c.glBufferData(c.GL_ARRAY_BUFFER, @intCast(c_long, self.cells.len * @sizeOf(CellVertex)), self.cells.ptr, c.GL_STREAM_DRAW);
        self.is_dirty = false;
    }

    // Enable alpha blending.
    c.glEnable(c.GL_BLEND);
    c.glBlendFunc(c.GL_SRC_ALPHA, c.GL_ONE_MINUS_SRC_ALPHA);

    // Enable shader
    c.glUseProgram(self.shader);

    // Bind texture
    c.glActiveTexture(c.GL_TEXTURE0);
    c.glBindTexture(c.GL_TEXTURE_2D_ARRAY, self.tileset.texture);

    // Draw the shit
    c.glBindVertexArray(self.vao);
    c.glDrawArraysInstanced(c.GL_POINTS, 0, 1, @intCast(c_int, self.cells.len));
}

/// Changes a given cell's tile and color.
pub fn updateCell(self: *Self, x: usize, y: usize, layer: usize, tile: u8, color: [3]u8) !void {
    const i = x + y * self.grid_width + layer * self.grid_width * self.grid_height;
    if (i > self.cells.len) {
        return error.PositionOutOfBounds;
    }
    self.cells[i].tile = tile;
    self.cells[i].color = color;

    self.is_dirty = true;
}

/// Tileset data and array texture.
pub const Tileset = struct {
    texture: c_uint,
    width: c_int,
    height: c_int,
};

/// Loads tilesets from the given folder path.
fn loadTilesets(path: []const u8, allocator: *Allocator) !Tileset {
    var tileset: ?Tileset = null;

    c.stbi_set_flip_vertically_on_load(1);

    var i: u16 = 0;
    while (i < 256) : (i += 1) {
        const file = try std.fmt.allocPrintZ(allocator, "{s}/{d}.png", .{ path, i });
        defer allocator.free(file);

        var width: c_int = undefined;
        var height: c_int = undefined;
        var channels: c_int = undefined;
        if (c.stbi_load(file, &width, &height, &channels, 4)) |image| {
            defer c.stbi_image_free(image);

            if (tileset == null) {
                // Create and set up texture.
                var texture: c_uint = undefined;
                c.glGenTextures(1, &texture);
                c.glBindTexture(c.GL_TEXTURE_2D_ARRAY, texture);
                c.glTexImage3D(c.GL_TEXTURE_2D_ARRAY, 0, c.GL_RGBA8, width, height, 256, 0, c.GL_RGBA, c.GL_UNSIGNED_BYTE, null);
                c.glTexParameteri(c.GL_TEXTURE_2D_ARRAY, c.GL_TEXTURE_MIN_FILTER, c.GL_NEAREST);
                c.glTexParameteri(c.GL_TEXTURE_2D_ARRAY, c.GL_TEXTURE_MAG_FILTER, c.GL_NEAREST);
                c.glTexParameteri(c.GL_TEXTURE_2D_ARRAY, c.GL_TEXTURE_WRAP_S, c.GL_CLAMP_TO_EDGE);
                c.glTexParameteri(c.GL_TEXTURE_2D_ARRAY, c.GL_TEXTURE_WRAP_T, c.GL_CLAMP_TO_EDGE);

                tileset = Tileset{
                    .texture = texture,
                    .width = width,
                    .height = height,
                };
            }

            if (width == tileset.?.width or height == tileset.?.height) {
                c.glTexSubImage3D(c.GL_TEXTURE_2D_ARRAY, 0, 0, 0, @intCast(c_int, i), width, height, 1, c.GL_RGBA, c.GL_UNSIGNED_BYTE, image);
            }
        }
    }

    if (tileset) |*ts| {
        ts.width = @divExact(ts.width, 16);
        ts.height = @divExact(ts.height, 16);
        return ts.*;
    } else {
        return error.NoImagesLoaded;
    }
}
